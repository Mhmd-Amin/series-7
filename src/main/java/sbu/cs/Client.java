package sbu.cs;

import java.io.FileNotFoundException;
import java.net.Socket;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.IOException;

public class Client {
    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    private final int PORT_NUMBER = 5050;
    private final String HOST_NAME = "localhost";
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private FileInputStream fileInputStream;

    public Client(String[] args) {
        if(!Helper.checkArgsSize(args)) {
            throw new IllegalArgumentException("You haven`t entered pathFile");
        }
        String fileAddress = args[0];

        try {
            socket = new Socket(HOST_NAME, PORT_NUMBER);
        } catch (IOException ioe) {
            System.err.println("Server not found.");
            ioe.printStackTrace();
        }

        try {
            sendFile(fileAddress);
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void sendFile(String filePath) throws IOException {
        try {
            fileInputStream = new FileInputStream(filePath);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            long fileSize = getFileSize(filePath);
            byte[] buffer = new byte[4096];
            String fileFormat = Helper.getFileFormat(filePath);


            System.out.println("Sending details...");
            dataOutputStream.writeUTF(fileFormat);
            dataOutputStream.writeLong(fileSize);
            System.out.println("Sending file...");
            while (fileInputStream.read(buffer) > 0) {
                dataOutputStream.write(buffer);
            }
            System.out.println("File sent.");
        }catch (FileNotFoundException ffe) {
            ffe.printStackTrace();
        }

        if (fileInputStream != null) {
            fileInputStream.close();
        }
        if (dataOutputStream != null) {
            dataOutputStream.close();
        }
    }

    private long getFileSize(String filePath) {
        long fileSize = 0;
        Path path = Paths.get(filePath);

        try {
            fileSize = Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileSize;
    }

    public static void main(String[] args) {
        Client client = new Client(args);
    }
}