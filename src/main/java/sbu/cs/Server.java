package sbu.cs;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Server extends Thread {
    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    private ServerSocket serverSocket;
    private Socket socket;
    private DataInputStream dataInputStream;
    private FileOutputStream fileOutputStream;

    public Server(int port, String[] args) {
        if (!Helper.checkArgsSize(args)) {
            throw new IllegalArgumentException("You have not entered pathFile.");
        }
        System.out.println("Establishing server...");
        try {
            serverSocket = new ServerSocket(port);
            runServer(args[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runServer(String fileAddress) {
        System.out.println("Waiting for a client...");
        try {
            socket = serverSocket.accept();
            System.out.println("A client connected.");
            saveFile(fileAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveFile(String filePath) throws IOException {
        dataInputStream = new DataInputStream(socket.getInputStream());
        byte[] buffer = new byte[4096];
        int read = 0;
        long fileSize, remaining, totalRead = 0;
        String inputFormat, outputFormat;

        outputFormat = Helper.getFileFormat(filePath);
        System.out.println("receiving file details...");
        inputFormat = dataInputStream.readUTF();
        if (!checkFileFormat(inputFormat, outputFormat)) {
            throw new IOException("File format is not match");
        }

        fileSize = dataInputStream.readLong(); // Send file size in separate msg
        remaining = fileSize;
        System.out.println("details received");

        System.out.println("receiving file...");
        System.out.print("[");
        fileOutputStream = new FileOutputStream(filePath);
        int present = 0;
        while((read = dataInputStream.read(buffer, 0, (int) Math.min((long) buffer.length, remaining))) > 0) {
            totalRead += read;
            remaining -= read;
            int temp = getPresent(totalRead, fileSize);
            fileOutputStream.write(buffer, 0, read);
            printPresent(present, temp);
            present = temp;
        }
        System.out.println("] 100%");
        System.out.println("File received and saved in " + filePath);

        fileOutputStream.close();
        dataInputStream.close();
    }

    private int getPresent(long totalRead, long fileSize) {
        long present = totalRead * 100 / fileSize;
        return (int) present;
    }

    private void printPresent(int present, int present1) {
        for (int i = present; i < present1; i++) {
            System.out.print("#");
        }
    }

    private boolean checkFileFormat(String inputFormat, String outputFormat) {
        if (inputFormat.equals(outputFormat)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Server server = new Server(5050, args);
    }
}
