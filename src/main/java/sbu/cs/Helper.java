package sbu.cs;

public class Helper {
    public static String getFileFormat(String pathFile) {
        String[] str;
        str = pathFile.split("\\.");
        if (str.length == 1) {
            return "null";
        }
        else {
            return str[str.length - 1];
        }
    }

    public static boolean checkArgsSize(String[] arg) {
        return arg.length == 1;
    }
}
